#!/bin/bash -ex

# this script will be called by the tuxtrigger in the following format:
# subprocess.run([script_path, repo_url, branch, new_sha]

## TODO: tuxtrigger does not support --private yet
## it needs to use tuxplan to workaround it for now
# tuxsuite keys add --type pat --domain gitlab.com --username liuyq1 --token "${LTSK_PRIVATE}"
tuxsuite keys update --type pat --domain gitlab.com --username "${LTSK_GITLAB_USER}" --token "${LTSK_GITLAB_TOKEN}"

dir_workspace="$(pwd)"

# Where the LTS updates to be fetched, normally it would something like this be:
#    git_repo="https://gitlab.com/ltsk/kernels/hikey-4.4"
source_url="${1}"
# The upstream branches to be tracked, which are:
source_branch="${2}"
# The new commit sha from the upstream branch, not used here
# shellcheck disable=SC2034
source_sha="${3}"

## configurations based on repo and branch
f_tuxsuite_plan_config=""
build_set_name=""
case "X${source_url}#${source_branch}X" in
    "Xhttps://gitlab.com/ltsk/kernels/hikey-4.4#lts-4.4.y-hikeyX")
        f_tuxsuite_plan_config="${dir_workspace}/plans/lts-4.4-hikey.yaml"
        build_set_name="hikey-4.4-lts"
        ;;
    "X*#*X")
        echo "${source_url} ${source_branch} is not supported yet"
        exit 1
        ;;
esac
# examples of tuxsuite plan
tuxsuite plan \
        --private \
        --git-repo "${source_url}" \
        --git-ref "${source_branch}" \
        "${f_tuxsuite_plan_config}" \
        --job-name "${build_set_name}" \
        --json-out "build.json" \
        --show-logs
