#!/bin/bash -ex

# install the jq for parsing json files to the callback url
apt-get update && apt-get install -y jq

dir_workspace="$(pwd)"


if [ -f "scripts/ltsk-common-bash-function.sh" ]; then
    # shellcheck source=/dev/null
    source scripts/ltsk-common-bash-function.sh

    echo "Need to register QA callback for the gitlab job"
    gitlab_callback_url=$(find_callback_job_url_from_pipeline "${CALLBACK_JOB_NAME}")
    echo "Callback URL: [${gitlab_callback_url}]"
fi

if [ -n "${ARTIFACTORIAL_TOKEN}" ]; then
    if tuxsuite keys get|grep -q ARTIFACTORIAL_TOKEN; then
        tuxsuite keys update --type variables:env ARTIFACTORIAL_TOKEN="${ARTIFACTORIAL_TOKEN}"
    else
        tuxsuite keys add --type variables:env ARTIFACTORIAL_TOKEN="${ARTIFACTORIAL_TOKEN}"
    fi
fi
if [ -n "${AP_SSID}" ]; then
    if tuxsuite keys get|grep -q AP_SSID; then
        tuxsuite keys update --type variables:env AP_SSID="${AP_SSID}"
    else
        tuxsuite keys add --type variables:env AP_SSID="${AP_SSID}"
    fi
fi
if [ -n "${AP_KEY}" ]; then
    if tuxsuite keys get|grep -q AP_KEY; then
        tuxsuite keys update --type variables:env AP_KEY="${AP_KEY}"
    else
        tuxsuite keys add --type variables:env AP_KEY="${AP_KEY}"
    fi
fi

opt_callback_headers=""
opt_callback_url=""
if [ -n "${gitlab_callback_url}" ]; then
    opt_callback_url="--callback-url ${gitlab_callback_url}"
    if [ -n "${REGISTER_CALLBACK_TOKEN}" ]; then
        opt_callback_headers="--callback-headers PRIVATE-TOKEN:${REGISTER_CALLBACK_TOKEN}"
    fi
fi
##########################################################################
### TODO: generate the tuxtrigger configuration file and the tuxplan
### configuration file dynamically with one file, like the android
### build configuration file.
### one challenging place is the support for the cts / vts parameters
##########################################################################

function generate_tuxtrigger_config(){
    local f_tuxtriger_config=${1}
    ## TEST_LAVA_SERVER=https://lkft.validation.linaro.org/RPC2/
    local url_lava_server=${TEST_LAVA_SERVER//\/RPC2*/}

    cat > "${f_tuxtriger_config}" <<__EOF__
repositories:
- url: ${KERNEL_REPO}
  squad_group: "${TEST_QA_SERVER_TEAM}"
  branches:
    - name: ${KERNEL_BRANCH}
      squad_project: ${TEST_QA_SERVER_PROJECT}
      plan: ${ANDROID_BUILD_CONFIG}.yaml
      lab: ${url_lava_server}
      lava_test_plans_project: lkft-android

__EOF__

}

function generate_tuxtrigger_plan(){
    local f_tuxtriger_config="${1}"
    bazel_target="//common:${BUILD_KERNEL_BAZEL_TARGET}"
    if echo "${BUILD_KERNEL_BAZEL_TARGET}"|grep '^//'; then
        bazel_target="${BUILD_KERNEL_BAZEL_TARGET}"
    fi

    ## https://gitlab.com/Linaro/tuxsuite/-/blob/master/examples/test-on-real-hardware/dragonboard-845c/dragonboard-845c-android-build-boot-plan.yaml
    cat > "${f_tuxtriger_config}" <<__EOF__
version: 1
name: Android Kernel Building for android-mainline
description: Building kernels for the Android and testing them
jobs:
- name: Build GKI and db845c on android-mainline
  bakes:
    - container: tuxbake/ubuntu-20.04
      sources:
        android:
          url: ${KERNEL_MANIFEST_REPO}
          bazel: true
          branch: ${KERNEL_MANIFEST_BRANCH}
          build_config: ${bazel_target}
          manifest: default.xml

  tests:
__EOF__

    device_types="${TEST_DEVICE_TYPES}"
    test_names="${TEST_TEMPLATES}"
    lava_job_priority="high"
    for device_type in ${device_types}; do
        for test_name in ${test_names}; do

            cat >> "${f_tuxtriger_config}" <<__EOF__
    - device: ${device_type}
      tests: [${test_name}]
      lava_test_plans_project: lkft-android
      lab: https://lkft.validation.linaro.org
      parameters:
        REFERENCE_BUILD_URL: ${REFERENCE_BUILD_URL}
        BUILD_REFERENCE_IMAGE_GZ_URL: "\$BUILD/Image.gz"
        TUXSUITE_BAKE_VENDOR_DOWNLOAD_URL: "\$BUILD/"
        LKFT_BUILD_CONFIG: "${ANDROID_BUILD_CONFIG}"
        ANDROID_VERSION: "${ANDROID_VERSION}"
        LAVA_JOB_PRIORITY: "${lava_job_priority}"
__EOF__

            if [ -n "${REFERENCE_BUILD_URL_GSI}" ]; then
                cat >> "${f_tuxtriger_config}" <<__EOF__
        REFERENCE_BUILD_URL_GSI: "${REFERENCE_BUILD_URL_GSI}"
__EOF__
            fi

            if [ "X${IMAGE_SUPPORTED_VENDOR_BOOT}" == "Xtrue" ]; then
                cat >> "${f_tuxtriger_config}" <<__EOF__
        IMAGE_SUPPORTED_VENDOR_BOOT: "${IMAGE_SUPPORTED_VENDOR_BOOT}"
__EOF__
            fi

            if echo "${test_name}"|grep vts; then
                vts_verion=$(echo "${TEST_VTS_URL}"| rev | cut -d'/' -f1,2 | rev)
                cat >> "${f_tuxtriger_config}" <<__EOF__
        TEST_VTS_URL: "${TEST_VTS_URL}"
        TEST_VTS_VERSION: "${vts_verion}"
__EOF__
            fi

            if echo "${test_name}"|grep cts; then
                cts_verion=$(echo "${TEST_CTS_URL}"| rev | cut -d'/' -f1,2 | rev)
                cat >> "${f_tuxtriger_config}" <<__EOF__
        TEST_CTS_URL: "${TEST_CTS_URL}"
        TEST_CTS_VERSION: "${cts_verion}"
        xts_test_params: "cts --include-filter CtsLibcoreTestCases --disable-reboot"
__EOF__
            fi

        done
    done
}

############################################################################################
REPO_ANDROID_BUILD_CONFIGS="https://android-git.linaro.org/git/android-build-configs"
git clone -b lkft "${REPO_ANDROID_BUILD_CONFIGS}" "${dir_workspace}/android-build-configs"
f_android_build_config="${dir_workspace}/android-build-configs/lkft/${ANDROID_BUILD_CONFIG}"
# shellcheck disable=SC1090
source "${f_android_build_config}"

dir_plans="${dir_workspace}/plans"
f_tuxtriger_plan="${dir_plans}/${ANDROID_BUILD_CONFIG}.yaml"
generate_tuxtrigger_plan "${f_tuxtriger_plan}"
f_tuxtriger_config="${dir_workspace}/configs/${ANDROID_BUILD_CONFIG}.yaml"
generate_tuxtrigger_config "${f_tuxtriger_config}"

#f_tuxtriger_config="${dir_workspace}/${TUXTRIGGER_CONFIG}"
# shellcheck disable=SC2086
tuxtrigger "${f_tuxtriger_config}" \
        --plan "${dir_plans}" \
        ${opt_callback_url} \
        ${opt_callback_headers} \
        --submit=change \
        --output "${DIR_ARTIFACTS}/output_file.yaml" \
        --json-out "${DIR_ARTIFACTS}" \
        --log-level=INFO --log-file "${DIR_ARTIFACTS}/log.txt"

##########################################################################
### TODO: create one file to include all necessary configurations.
### Like the branch, report name, git describe, etc
### so that other places won't need parse the same information again
##########################################################################
