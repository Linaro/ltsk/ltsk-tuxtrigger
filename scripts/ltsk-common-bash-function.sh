#!/bin/bash

##########functions definition########################
# needs to be run within the docker container of squadproject/squad-report:1.2.0
# which has the build-email and send-mail command installed by default
function send_mail_common(){
    local f_email_body="${1}"
    local email_subject="${2}"

    if [ -n "${REVIEW_TO_NAME}" ]; then
        REAL_REVIEW_TO=$(eval "echo \$${REVIEW_TO_NAME}")
        if [ -n "${REAL_REVIEW_TO}" ]; then
            export REVIEW_TO="${REAL_REVIEW_TO}"
        fi
    fi

    if [ -n "${REVIEW_CC_NAME}" ]; then
        REAL_REVIEW_CC=$(eval "echo \$${REVIEW_CC_NAME}")
        if [ -n "${REAL_REVIEW_CC}" ]; then
            export REVIEW_CC="${REAL_REVIEW_CC}"
        fi
    fi

    build-email \
        --msg-from="${REVIEW_FROM}" \
        --msg-to="${REVIEW_TO}" \
        --msg-cc="${REVIEW_CC}" \
        --msg-body="${f_email_body}" \
        --msg-subject="${email_subject}" \
        --msg-body-pre="Go here to check the pipeline jobs: ${CI_PIPELINE_URL}." >mail.txt

    send-email mail.txt
}

function find_callback_job_url_from_pipeline(){
    local callback_job_name="${1}"
    local project_id="${2}"
    local pipeline_id="${3}"
    if [ -z "${callback_job_name}" ]; then
        return
    fi

    if [ -z "${project_id}" ]; then
        project_id="${CI_PROJECT_ID}"
    fi
    if [ -z "${pipeline_id}" ]; then
        pipeline_id="${CI_PIPELINE_ID}"
    fi

    # NOTE1:
    #   https://docs.gitlab.com/ee/api/jobs.html
    #   when all the callback jobs finished, there will be no jobs returned with "scope[]=manual" specified
    #   scope is the status, will be changed when the job is running or finished, etc
    #   here does not specify the scope to have all jobs returned, and the callback
    #   could be registered again, even it's called before
    # NOTE#2
    #   https://docs.gitlab.com/ee/api/index.html#pagination-link-header
    #   here set to use the max 100 for the per_page, assuming there won't be pipeline that has more than 100 jobs
    #   For that case, we need either update here or re-design the pipeline jobs
    local f_pipeline_jobs="pipelines-jobs-${pipeline_id}.json"
    if [ -n "${ACCESS_TO_GITLAB_PRIVATE_PIPELINES}" ] && [ "${ACCESS_TO_GITLAB_PRIVATE_PIPELINES}" = "True" ] && [ -n "${GITLAB_TOKEN}" ]; then
        curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/${project_id}/pipelines/${pipeline_id}/jobs?per_page=100" -o "${f_pipeline_jobs}"
    else
        curl "https://gitlab.com/api/v4/projects/${project_id}/pipelines/${pipeline_id}/jobs?per_page=100" -o "${f_pipeline_jobs}"
    fi
    job_id=$(jq -r ".[] | select(.name == \"${callback_job_name}\") | .id" "${f_pipeline_jobs}")
    test -n "${job_id}" # add check for the job_id to avoid generating incorrect callback_url
    callback_url="https://gitlab.com/api/v4/projects/${project_id}/jobs/${job_id}/play"
    echo "${callback_url}"
}
######### functions definiton end here ####################################
