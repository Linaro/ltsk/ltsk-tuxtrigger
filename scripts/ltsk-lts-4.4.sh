#!/bin/bash -ex

## TODO: tuxtrigger does not support --private yet
## it needs to use tuxplan to workaround it for now

dir_workspace="$(pwd)"
f_tuxsuite_plan_config="${dir_workspace}/plans/lts-4.4-hikey.yaml"
f_tuxtriger_config="${dir_workspace}/${TUXTRIGGER_CONFIG}"
dir_plans="${dir_workspace}/plans"
tuxtrigger "${f_tuxtriger_config}" \
        --plan "${dir_plans}" \
        --sha-compare=squad --submit=change \
        --output "${dir_workspace}/output_file.yaml" \
        --log-level=info --log-file "${dir_workspace}/log.txt"

#tuxtrigger "${TUXTRIGGER_CONFIG}" \
#        --plan-disabled \
#        --pre-submit "${dir_workspace}/scripts/ltsk-lts-4.4-tuxplan.sh" \
#        --plan "${dir_workspace}" \
#        --sha-compare=squad --submit=change \
#        --output "${dir_workspace}/output_file.yaml" \
#        --log-level=info --log-file "${dir_workspace}/log.txt"
