#!/bin/bash -ex

###########################################################
kernel_branch="${1}"
trim__number=1000
regressions_number=1000

if [ -z "${kernel_branch}" ]; then
    echo "Please specify the kernel branch like the following"
    echo "$0 \${kernel_branch}"
    exit 1
fi

if [ -f "scripts/ltsk-common-bash-function.sh" ]; then
    # shellcheck source=/dev/null
    source scripts/ltsk-common-bash-function.sh
fi


LKFT_WORK_DIR=$(pwd)
DIR_ARTIFACTS=${DIR_ARTIFACTS:-${LKFT_WORK_DIR}}
mkdir -p "${LKFT_WORK_DIR}" "${DIR_ARTIFACTS}"

SQUAD_URL="https://qa-reports.linaro.org"
SQUAD_GROUP="${SQUAD_GROUP:-ltsk}"
report_type="android"

if [ ! -d "${LKFT_WORK_DIR}/squad-report" ]; then
    git clone --depth 1 -b ltsk https://gitlab.com/Linaro/lkft/users/yongqin.liu/squad-report.git "${LKFT_WORK_DIR}/squad-report"
fi
if [ ! -d "${LKFT_WORK_DIR}/workspace" ]; then
    # python3-virtualenv package does not work
    apt-get update && apt-get install -y virtualenv
    virtualenv --python=python3 "${LKFT_WORK_DIR}/workspace"
    # shellcheck source=/dev/null
    source "${LKFT_WORK_DIR}"/workspace/bin/activate
    cd "${LKFT_WORK_DIR}/squad-report" && pip --default-timeout=1000  install -r requirements.txt && python setup.py install && cd -
else
    # shellcheck source=/dev/null
    source "${LKFT_WORK_DIR}"/workspace/bin/activate
fi

f_report_config="${LKFT_WORK_DIR}/squad-report-config.yml"
if [ ! -f "${f_report_config}" ]; then
    wget -O "${f_report_config}" "https://gitlab.com/Linaro/lkft/pipelines/lkft-common/-/raw/master/squad-report-config.yml"
fi
f_flakefile="${LKFT_WORK_DIR}/squad-report/squad_report/androidreport-flakey.txt"

### get the kernel build version from the plan output file name
### created by the tuxtrigger command
if [ -n "${SQUAD_PROJECT}" ]; then
    # e.g. artifacts/android15-6.1-test_ASB-2023-07-05_14-6.1-8048-gb9f900cc8927.json
    f_plan_output=$(ls "${DIR_ARTIFACTS}"/"${SQUAD_PROJECT}"_*.json || true)
    if [ -n "${f_plan_output}" ]; then
        f_basename=$(basename "${f_plan_output}")
        kernel_label=$(echo "${f_basename}"|sed "s|${SQUAD_PROJECT}_||"|sed "s|.json||")
    else
        echo "No tuxplan output file generated, which is probably the no changes case"
        exit 0
    fi
fi
if [ -n "${kernel_label}" ]; then
    f_report="${DIR_ARTIFACTS}/androidreport-${kernel_branch}-${kernel_label}.txt"
    opt_build="--build=${kernel_label}"
else
    f_report="${DIR_ARTIFACTS}/androidreport-${kernel_branch}.txt"
    opt_build=""
fi
for i in {1..5}; do
    echo "Try for the $i times to generate the report"
    # shellcheck disable=SC2086
    ${LKFT_WORK_DIR}/squad-report/squad-report \
                    --url="${SQUAD_URL}" \
                    --report-kernel "${kernel_branch}" \
                    ${opt_build} \
                    --config "${f_report_config}" \
                    --config-report-type="${report_type}" \
                    --flakefile "${f_flakefile}" \
                    --trim-number "${trim__number}" \
                    --output "${f_report}"
    if [ -f "${f_report}" ]; then
        break
    fi
done
echo "Please check the file for details: ${f_report}"
deactivate


if [ -n "${REGRESSIONS_NUMBER_TO_BE_CHECKED}" ]; then
    regressions_number="${REGRESSIONS_NUMBER_TO_BE_CHECKED}"
fi

generate_report_failed=false
found_huge_regressions=false
found_failed_project=false
found_critical_error=false
found_infra_error=false

if [ ! -f "${f_report}" ]; then
    echo "Failed to generate the report for kernel: ${kernel_branch} ${kernel_label} but failed" > "${f_report}"
    generate_report_failed=true
else
    # shellcheck disable=SC1004
    if grep 'Regressions of' "${f_report}" | grep '^ ' | \
        awk -v regressions_number="${regressions_number}" \
            'BEGIN { huge_regressions=0; } \
            {if ($1 + 0 > regressions_number) {huge_regressions=huge_regressions + 1;}} \
            END {if ( huge_regressions >= 1 ){ exit 0;} else { exit 1; }}'; then
        found_huge_regressions=true
    fi

    if grep 'NOTE: project' "${f_report}"; then
        found_failed_project=true
    fi

    if grep 'CRITICAL ERROR' "${f_report}"; then
        found_critical_error=true
    fi

    if grep 'INFRA ERROR' "${f_report}"; then
        found_infra_error=true
    fi
fi

if ${generate_report_failed} || ${found_failed_project} || $found_huge_regressions; then
    # send to REVIEW_CC_ISSUE when not empty, otherwise send to REVIEW_TO
    if [ -n "${REVIEW_TO_ISSUE}" ]; then
        export REVIEW_TO_NAME="REVIEW_TO_ISSUE"
    fi
    # send to REVIEW_CC_ISSUE when not empty, otherwise send to REVIEW_CC
    if [ -n "${REVIEW_CC_ISSUE}" ]; then
        export REVIEW_CC_NAME="REVIEW_CC_ISSUE"
    fi
    report_key="ltsk-android-report"
    if echo "${kernel_branch}" | grep sanity; then
        report_key="lkft-android-report-sanity"
    fi
    if ${generate_report_failed}; then
        msg_subject="[${report_key}] [issue-noreport] ${kernel_branch} | ${kernel_label}"
    elif ${found_critical_error} && ${found_failed_project}; then
        msg_subject="[${report_key}] [issue-incomplete-critical] ${kernel_branch} | ${kernel_label}"
    elif ${found_infra_error} && ${found_failed_project}; then
        msg_subject="[${report_key}] [issue-incomplete-infra] ${kernel_branch} | ${kernel_label}"
    elif ${found_huge_regressions} && ${found_failed_project}; then
        msg_subject="[${report_key}] [issue-incomplete-regressions] ${kernel_branch} | ${kernel_label}"
    elif ${found_failed_project}; then
        msg_subject="[${report_key}] [issue-incomplete] ${kernel_branch} | ${kernel_label}"
    elif ${found_huge_regressions}; then
        msg_subject="[${report_key}] [issue-regressions] ${kernel_branch} | ${kernel_label}"
    else
        # should not be able to happen
        msg_subject="[${report_key}] [issue-unknown] ${kernel_branch} | ${kernel_label}"
    fi
else
    msg_subject="[ltsk-android-report] ${kernel_branch} | ${kernel_label}"
fi

send_mail_common "${f_report}" "${msg_subject}"
