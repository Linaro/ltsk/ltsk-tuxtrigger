#!/bin/bash -ex

## The following environments need to be defined:
##  SQUAD_PROJECT: used to find the tuxplan output json file
##  CALLBACK_JOB_NAME: used to find the gitlab job play url
##  REGISTER_CALLBACK_TOKEN: used to run the curl command to plan the job

# install the jq for parsing json files to the callback url
apt-get update && apt-get install -y jq

dir_workspace="$(pwd)"

if [ ! -f "scripts/ltsk-common-bash-function.sh" ]; then
    echo "ltsk-common-bash-function.sh is not found, please check and try again"
    exit 1
fi
# shellcheck source=/dev/null
source scripts/ltsk-common-bash-function.sh

echo "Need to register QA callback for the gitlab job"
gitlab_callback_url=$(find_callback_job_url_from_pipeline "${CALLBACK_JOB_NAME}")
echo "Callback URL: [${gitlab_callback_url}]"

if [ -z "${gitlab_callback_url}" ]; then
    echo "The callback url is not found for the job: ${CALLBACK_JOB_NAME}"
    exit 0
fi

if [ -z "${REGISTER_CALLBACK_TOKEN}" ]; then
    echo "The gitlab token REGISTER_CALLBACK_TOKEN is not defined, please check and try again"
    exit 0
fi

# need SQUAD_PROJECT to find the tuxplan output file
if [ -n "${SQUAD_PROJECT}" ]; then
    # e.g. artifacts/android15-6.1-test_ASB-2023-07-05_14-6.1-8048-gb9f900cc8927.json
    f_plan_output=$(ls "${DIR_ARTIFACTS}"/"${SQUAD_PROJECT}"_*.json || true)
    if [ -z "${f_plan_output}" ]; then
        echo "No tuxplan output file generated, which is probably the no changes case"
        echo "Trigger the callback job here instead of wait for the call from squad side."
        curl --request POST --header "PRIVATE-TOKEN:${REGISTER_CALLBACK_TOKEN}" "${gitlab_callback_url}"
    fi
fi
