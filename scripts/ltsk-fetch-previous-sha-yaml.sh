#!/bin/bash -ex

if [ -n "${LTSK_IGNORE_COMMIT_SHA}" ]; then
    echo "Ignore the check for the previous commit sha, trigger a new build anyway"
    exit 0
fi
# Install curl and jq for API requests and JSON parsing
apt-get update && apt-get install -y curl unzip

cmd_curl="curl -fsSL "${CI_PROJECT_URL}/-/jobs/artifacts/main/download?job=${CI_JOB_NAME}" --output artifacts.zip"
if ${cmd_curl}; then
    unzip -o artifacts.zip artifacts/output_file.yaml || true
else
    echo "Failed to download previous artifacts.zip to check the commit SHA information"
    echo "Builds and Tests will be triggered anyway"
fi
